#identify all athletes that ran 800, 1600, and 3200 all 4 years, retrieve AtheleteID
#identify top time for each year for each athlete - retreive time, date, eventID
#plot time progression for each distance by gender

#import os
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output
import plotly.graph_objs as go
import plotly.figure_factory as ff
import numpy as np
from google.cloud import datastore

from app import app

#server = Flask(__name__)
#app = dash.Dash(__name__, server=server)
#app = dash.Dash(__name__)

#server = app.server # the Flask app

#my_css_url = 'https://codepen.io/chriddyp/pen/bWLwgP.css'

#app.css.append_css({"external_url": my_css_url})

#es = Elasticsearch(hosts=['elasticsearch1', 'elasticsearch2'])

def create_client(project_id):
    return datastore.Client(project_id)

project_id = 'dist-app-1'
client = create_client(project_id)

def racetime(x, no_dec=False):
    from datetime import timedelta
    import numpy as np
    if np.isnan(x): return np.NAN
    d = timedelta(seconds=x/1000)
    [h, m, s] = str(d).split(':')
    s = s.split('.')[0]
    x = d.microseconds/100000
    if no_dec:
        return str(m) + ':' + str(s)
    else:
        return str(m) + ':' + str(s) + '.' + str(x)
    
def racedatetime(x):
    from datetime import datetime
    import numpy as np
    if np.isnan(x): 
        return np.NAN
    else:
        return datetime.fromtimestamp(x/1000.0)

def time_to_units(timestr):
    import numpy as np
    try:
        ftr = [60,1]
        return int(sum([a*b for a,b in zip(ftr, map(float,timestr.split(':')))])*1000)
    except:
        return np.NAN

def get_results(event, best_time_str, best_time_yr, similarity, gender):
    
    import numpy as np
    
    hs_years = ['Fr', 'So', 'Jr', 'Sr']
    try:
        yr_index = hs_years.index(best_time_yr)
    except:
        print 'year abbreviation entered incorrectly, use Fr, So, or Jr'
        return []

    if yr_index == 3:
        print 'year for best time must be Jr or lower'
        return []
    
    best_units = time_to_units(best_time_str)
    
    if np.isnan(best_units):  # return None if time is malformed
        print 'time entered incorrectly'
        return []
    
    grade = best_time_yr + '_Units'
    
    print 'querying for %s, %s, %s, %s' %(event, gender, grade, best_units)
    
    # google datastore
    kind = 'Runner-Event'
    query = client.query(kind=kind)
    query.add_filter('EventCode', '=', event)
    query.add_filter('Gender', '=', gender)
    query.add_filter(grade, '<=', best_units + int(similarity * 1000))
    query.add_filter(grade, '>=', best_units - int(similarity * 1000))
    results = query.fetch()
    
    return results

layout = html.Div([
    dcc.Link('Go to Index', href='/'),
    html.Div(),
    html.Div([
        html.Div([
            html.H1(
                'High School Distance Race Time Projections',
#                className='eight columns',
                style={'text-align': 'center'},),],),
        html.Div([
            html.Label('Race Length'),
            dcc.Dropdown(
                id='race',
                options=[{'label': i, 'value': i} for i in ['800m', '1600m', '3200m']],
                value='1600m'
            )
        ],
            style={'width': '150px', 'display': 'inline-block', 'margin': '2%'}),
        html.Div([
            html.Label('High School Grade'),
            dcc.Dropdown(
                id='grade',
                options=[{'label': i, 'value': i} for i in ['Fr', 'So', 'Jr']],
                value='Fr'
            )
        ],
            style={'display': 'inline-block'}),
        html.Div([
            html.Label('Gender'),
            dcc.Dropdown(
                id='gender',
                options=[{'label': i, 'value': i} for i in ['girls', 'boys']],
                value='girls'
            )
        ],
            style={'width': '100px', 'display': 'inline-block', 'margin': '2%'}),
        html.Div([
            html.Label('Best Time'),
            dcc.Input(
                id='time',
                placeholder='Enter a race time i.e, 5:05...',
                type='text',
                value='5:30'
            )
        ],
            style={'display': 'inline-block', 'margin': '2%'}),
        html.Div([
            html.Label('Range in Seconds'),
            dcc.Input(
                id='similarity',
                placeholder='Enter a time range i.e., 1.0 seconds',
                type='text',
                value='3.0'
            )
        ],
            style={'display': 'inline-block', 'margin': '2%'}),
        html.Div([
            html.Label('Plot Stats or Individual Runners'),
            dcc.RadioItems(
                id='bool_stats',
                options=[{'label': i, 'value': j} for i,j in zip(['Stats', 'Individuals'], [True, False])],
                value=True,
                labelStyle={'display': 'inline-block', 'margin': '2%'}
            )
        ],
            style={'display': 'inline-block'}),
    ]),
    
    dcc.Graph(id='historical_progression')
    
])

@app.callback(
    Output('historical_progression', 'figure'),
    inputs=
    [Input('race', 'value'),
     Input('grade', 'value'),
     Input('time', 'value'),
     Input('similarity', 'value'),
     Input('gender', 'value'),
     Input('bool_stats', 'value')
    ])
def update_graph(race, grade, time, similarity, gender, bool_stats=True):

    print "calling with time " + str(time)
                
    grades = ['Fr', 'So', 'Jr', 'Sr']
    
    grade_index = grades.index(grade)
    next_grade = grades[grade_index + 1]
    
    try: similarity = float(similarity)
    except: similarity = 1.0
           
    results = get_results(race, time, grade, similarity, gender)
    
    if results == []:
        return
    
    results_list = list(results)
    total_results = len(results_list)
    print "total hits = ", total_results
    
    if total_results == 0:
        return
    keys = results_list[0].keys()
    mask = ['_Units' in x for x in keys]
    keys_grade = [d for (d, s) in zip(keys, mask) if s]
    
    # create percentiles
    percentiles = [1,5,10,15,20,30,40,50,60,70,80,90]
    times = {}
    times_percent = {}
    for grade in grades:
        key = grade + "_Units"
        times[key] = [results_list[i][key] for i in xrange(len(results_list))]
        times[key] = [x for x in times[key] if x !='nan']  # remove nans from list
        times_percent[key] = [np.percentile(times[key], x) for x in percentiles]  # create percentiles

    # create plots

    if bool_stats:
        plot_dict = times_percent # plot percentilcs
        max_plots = len(percentiles)
    else:
        plot_dict = times  # plot actual times
        # limit plots to fit well on graph
        max_plots = total_results
        plot_limit = 100
        if total_results > plot_limit:
            max_plots = plot_limit
        
    keys = [x + '_Units' for x in grades]
    traces = []
    x_data=[0,1,2,3]
    #text_cols = [x + '_Time' for x in grades]
    for i in xrange(max_plots):
       raw_y = [plot_dict[k][i] for k in keys]
       y_data = [racedatetime(x) for x in raw_y]
       text_1 = [racetime(x,  no_dec=True) for x in raw_y]
       if bool_stats:
           text_2 = str(percentiles[i]) + '%tile: '
       else:
           text_2 = ''
       text = [text_2 + x for x in text_1]
       traces.append(go.Scatter(
           y=y_data,
            x=x_data,
            text=text,
            mode='lines',
            connectgaps=True,
            hoverinfo='text',
            xaxis='x2', yaxis='y2'))
     
    # create graphic - start with table


    percentile_list = [racetime(x) for x in times_percent[next_grade + '_Units']]
    temp = [list(x) for x in zip(*[percentiles[::-1], percentile_list[::-1]])]
    fig = ff.create_table([['Percentile', 'Time']] + temp, height_constant=30)

    # then add scatter plot
    fig['data'].extend(go.Data(traces))

    # Edit layout for subplots
    fig.layout.xaxis.update({'domain': [0, .2]})
    fig.layout.xaxis2.update(dict(domain=[0.3, 1.], title='Year in High School', ticktext=['Fr', 'So', 'Jr', 'Sr'], tickvals=[0,1,2,3]))

    # The graph's yaxis MUST BE anchored to the graph's xaxis
    fig.layout.yaxis2.update(dict(anchor='x2', title='Time', tickformat='%M:%S', hoverformat='%M:%S'))

    fig_title = 'Time Progression for Similar Runners With a Time of %s as a %s in the %s' %(time, grade, race)
    fig_subtitle = 'There are %s Similar Runners within +/- %s Seconds' %(total_results, similarity)
    table_title = 'Predicted Time for Next Year'
    fig.layout.update({'showlegend': False})
    fig.layout.margin.update({'t':50, 'b':50, 'r':25, 'l':25})
    fig.layout.annotations.extend([dict(x=.6, y= 1.15, xref='paper', yref='paper', text=fig_title, showarrow=False, font=dict(size=14))])
    fig.layout.annotations.extend([dict(x=.6, y= 1.08, xref='paper', yref='paper', text=fig_subtitle, showarrow=False)])
    fig.layout.annotations.extend([dict(x=.03, y= 1.15, xref='paper', yref='paper', text=table_title, showarrow=False, font=dict(size=14))])
    
    return fig
    
#if __name__ == '__main__':
#     app.run_server()