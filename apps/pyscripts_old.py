import requests
import pandas as pd
import json
import os
from elasticsearch import Elasticsearch

def insertDataframeIntoElastic(dataFrame,index='index', typ = 'test', server = 'http://localhost:9200',
                               chunk_size = 2000, ):
    headers = {'content-type': 'application/x-ndjson', 'Accept-Charset': 'UTF-8'}
    records = dataFrame.to_dict(orient='records')
    actions = ["""{ "index" : { "_index" : "%s", "_type" : "%s"} }\n""" % (index, typ) + json.dumps(records[j])
                    for j in range(len(records))]
    i=0
    while i<len(actions):
        serverAPI = server + '/_bulk' 
        data='\n'.join(actions[i:min([i+chunk_size,len(actions)])])
        data = data + '\n'
        r = requests.post(serverAPI, data = data, headers=headers)
        print 'Posted records %s through %s' %(i , (i+ chunk_size - 1))
        i = i+chunk_size
        
def read_race_data(year, path): 
# read event data file
    filename = os.path.join(path, 'crosscountry_' + str(year) + '_event_data_us_hs.json')
    df = pd.read_json(filename)
    df.Date = pd.to_datetime(df.Date)
    df['Year'] = year
    return df.drop_duplicates()
    
def read_handicap_file(year, path):
    handicap_file = str(year) + '_xc_event_handicaps_against_221756_2016.pickle'
    df = pd.read_pickle(os.path.join(path, handicap_file))
    df['Year'] = year
    return df[['sm_mean', 'Year']].reset_index()

def retrieve_df(year, path):
    file_name = str(year) + '_XC_data'
    file_path = os.path.join(path, file_name)
    df = pd.read_pickle(file_path)
    df['Year'] = year
    
    print ''
    print '***Loaded dataframe with the following characteristics***'
    print 'Number of Athletes =', len(list(set(df.AthleteID)))
    print 'Year =', str(year)
    
    return df

def racetime(x, no_dec=False):
    from datetime import timedelta
    import time
    import numpy as np
    if np.isnan(x): return np.NAN
    d = timedelta(seconds=x/1000)
    [h, m, s] = str(d).split(':')
    s = s.split('.')[0]
    x = d.microseconds/100000
    if no_dec:
        return str(m) + ':' + str(s)
    else:
        return str(m) + ':' + str(s) + '.' + str(x)

def racedatetime(x):
    from datetime import datetime
    import numpy as np
    if np.isnan(x):
        return np.NAN
    else:
        return datetime.fromtimestamp(x/1000.0)
        
def reformat_Range(interval):
    # converts pandas Interval to text field and reformats from milliseconds to minutes
    nums = [interval.left/60000, interval.right/60000]
    reform_text = '(' + str(nums[0]) + ', ' + str(nums[1]) + ']' 
    return reform_text
           
def json_to_df(json_list):
    # convert list of json objects to a pandas dataframe with standard col names
    return pd.DataFrame([x['_source'] for x in json_list])
           
# get teams within a state
def get_teams_in_state(state, es):
    # es = elasticsearch client
    # return dataframe with TeamID and TeamName
    # input state as string
    # output TeamID as int64 and TeamName as object in dataframe
    index_name = 'xc_teams'
    res = es.search(
        index=index_name, 
        #doc_type="track", 
        size=0,
        body={
              "query": {
                    "constant_score" : {
                        "filter" : {
                            "term": {
                                "State.keyword": state
                        }}
                            }},
                "aggs": {
                    "distinct_team_IDs": {
                        "terms": {
                            "field": "ID-Name.keyword",
                            "size": 10000
                    }
                }
            }

             })
    id_name_list = [x['key'] for x in res['aggregations']['distinct_team_IDs']['buckets']]
    temp_list = [x.split('%') for x in id_name_list]
    final_list = [[int(x[0]), x[1]] for x in temp_list]
    return pd.DataFrame(final_list, columns=['TeamID', 'TeamName'])


# get race name and date from index with race ID
def get_race_info(raceID, es):
    # es = elasticsearch client
    index_name = 'xc_meets'
    res = es.search(
        index=index_name,
        size=100,
        body={"size": 0,
              "query": {
                    "constant_score" : {
                        "filter" : {
                            "term": {
                                "EventID": raceID
                            }
                        }
                    }
                }
            }
		)
    try:
        record = res['hits']['hits'][0]['_source']
        return record['Event'], record['Date']
    except:
        return 'No race was found with EventID %s' %(raceID)
    
# get handicap info from index
def get_race_hcap(raceID, es):
    # es = elasticsearch client
    index_name = 'xc_handicaps'
    res = es.search(
        index=index_name, 
        size=100,
        body={"size": 0,
              "query": {
                    "constant_score" : {
                        "filter" : {
                            "term": {
                                "EventID": raceID
                            }
                        }
                    }
                }
              }
        )
    record = res['hits']['hits']
    #temp_hcaps = get_race_hcap(temp_df.iloc[0]['EventID'])
    try: 
        temp_df_hcap = json_to_df(record).set_index('Range')['sm_mean']
        return temp_df_hcap.sort_index()
    except:
        return None

# pull all races for an AthleteID for one year
def get_runner_perf_data(ath_ID, year, es):
    # es = elasticsearch client
    index_name = 'xc_data'
    res = es.search(
        index=index_name, 
        #doc_type="track", 
        size=100,
        body={"size": 0,
              "query": {
                    "constant_score" : {
                        "filter" : {
                            "bool" : {
                                "must" : [
                                    {"term": { "AthleteID": ath_ID }},
                                    {"term": { "Year": year}}
                                ]
                            }
                        }
                    }
                }
              }
        )
    record = res['hits']['hits']
    try:
        temp_df = json_to_df(record)
        return temp_df
    except:
        return None

def get_ath_IDs_for_team(teamID, year, es):
    # es = elasticsearch client
    index_name = 'xc_data'
    res = es.search(
        index=index_name, 
        #doc_type="track", 
        size=0,
        body={"size": 0,
              "query": {
                    "constant_score" : {
                        "filter" : {
                            "bool" : {
                                "must" : [
                                    {"term": { "TeamID": teamID }},
                                    {"term": { "Year": year}}
                                ]
                            }
                        }
                    }
                },
                "aggs": {
                    "distinct_ath_IDs": {
                        "terms": {
                            "field": "AthleteID",
                            "size": 1000
                    }
                }
            }

             })
    return [x['key'] for x in res['aggregations']['distinct_ath_IDs']['buckets']]

# pull name for AthleteID
def get_athlete_name(ath_ID, es):
    # es = elasticsearch client
    index_name = 'xc_data'
    res = es.search(
        index=index_name, 
		#doc_type="track",
        size=1,
        body={"size": 0,
              "query": {
                    "constant_score" : {
                        "filter" : {
                            "term": {
                                "AthleteID": ath_ID
                            }
                        }
                    }
                }
              }
        )
    record = res['hits']['hits'][0]['_source']
    return record['FirstName'] + ' ' + record['LastName']

# figure out which handicap applies to the runner
def create_bin_seq(df):
    # create the bin sequence from the handicap df
    bins = df.index.tolist()
    bin_sequence = []
    for text in bins:
        nums = [int(filter(str.isdigit, str(s))) for s in text.split()]
        bin_sequence.append(nums[0] * 60000)
    nums = [int(filter(str.isdigit, str(s))) for s in text.split()]
    bin_sequence.append(nums[1] * 60000)
    return bin_sequence

######## identify all races names ########
def get_race_names_for_year(state, year, es):
    # es = elasticsearch client
    # return dataframe with TeamID and TeamName
    # input state as string
    # output TeamID as int64 and TeamName as object in dataframe
    index_name = 'xc_meets'
    res = es.search(
        index=index_name, 
        #doc_type="track", 
        size=1000,
        body={
              "query": {
                    "constant_score" : {
                        "filter" : {
                            "bool" : {
                                "must" : [
                                    {"term": { "State.keyword": state }},
                                    {"term": { "Year": year}}
                                ]
                            }
                        }
                            }}

             })
    id_name_list = [x['_source'] for x in res['hits']['hits']]
    #temp_list = [x.split('%') for x in id_name_list]
    #final_list = [[int(x[0]), x[1]] for x in temp_list]
    df = pd.DataFrame(id_name_list, columns=['Date', 'Event', 'EventID'])
    df['EventID%Event%Date'] = df['EventID'].astype('str') + '%' + df['Event'] + '%' + df['Date']
    return df[['Event', 'EventID%Event%Date']]
    
######## pull race data with RaceID ########
def get_race_data(raceIDs_list, es):
    # raceIDs is a list of eventIDs
    # returns dataframe of event data
    index_name = 'xc_data'
    res = es.search(
        index=index_name, 
        size=5000,
        body={
              "query": {
                    "constant_score" : {
                        "filter" : {
                            "bool" : {
                                "must" : [
                                    {"terms": { "EventID": raceIDs_list }}
                                ]
                            }
                        }
                    }
                }
              }
        )
    record = res['hits']['hits']
    try:
        temp_df = json_to_df(record)
        return temp_df
    except:
        return None

######## identify all races with similar name ########
def get_races_by_name(name, year, state, date1, date2, es):
    index_name = 'xc_meets'
    res = es.search(
        index=index_name, 
        size=1,
        body={
              "query": {
                    "bool": {
                        "must": [
                             {"match": { "Event": name }},
                             {"term":  {'Year': year }},
                             {"term":  {'State.keyword': state }},
                             {"range": {
                                "Date" : {
                                    "gte" : date1,
                                    "lte" : date2                                        }
                                        }                                     
                                     }
                                ]
                                }
              }
            }
        )
    record = res['hits']['hits'][0]['_source']

    try:
        return record['Event'], record['EventID'], record['Date']
    except:
        return None
