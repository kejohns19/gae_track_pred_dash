import pandas as pd
from dash.dependencies import Input, Output
import dash_core_components as dcc
import dash_html_components as html
import plotly.graph_objs as go
from elasticsearch import Elasticsearch
import datetime
import pyscripts

from app import app

#endpoint = '35.196.248.48:9200'
endpoint = 'http://localhost:8001/api/v1/namespaces/default/services/elasticsearch:9200/proxy'
es = Elasticsearch(endpoint)

#app = dash.Dash(__name__)
#server = app.server # the Flask app

#my_css_url = 'https://codepen.io/chriddyp/pen/bWLwgP.css'
#app.css.append_css({"external_url": my_css_url})

#path = os.path.join(os.path.expanduser('~'), 'Google Drive', 'RaceAnalysis')


states = ['AL', 'AK', 'AZ', 'AR', 'CA', 'CO', 'CT', 'DE', 'FL', 'GA', 'HI', 'ID', 
             'IL', 'IN', 'IA', 'KS', 'KY', 'LA', 'ME' ,'MD', 'MA', 'MI', 'MN', 'MS', 
             'MO', 'MT', 'NE', 'NV', 'NH', 'NJ', 'NM', 'NY', 'NC', 'ND', 'OH', 'OK', 
             'OR', 'PA', 'RI', 'SC', 'SD', 'TN', 'TX', 'UT', 'VT', 'VA', 'WA', 'WV',
             'WI', 'WY']

layout = html.Div([
    dcc.Link('Go to Index', href='/'),
    html.Div(),
    html.Div([
        html.Div([
            html.H1(
                'High School Cross Country Race Time Adjustments',
                style={'text-align': 'center'},),],),

        html.Div([
            html.Label('Year'),
            dcc.Dropdown(
                id='year',
                options=[{'label': i, 'value': i} for i in ['2016', '2015', '2014']],
                value='2016'
            )], style={'width': '150px', 'display': 'inline-block', 'margin': '2%'}),

        html.Div([
            html.Label('State'),
            dcc.Dropdown(
                id='state',
                options=[{'label': i, 'value': i} for i in states],
                value='VA'
            )], style={'width': '250px', 'display': 'inline-block', 'margin': '2%'}),

        html.Div([
            html.Label('High School Name'),
            dcc.Dropdown(
                id='team-name',
                #options=[{'label': i, 'value': i} for i in team_names],
                #label='Tuscarora High School (VA)',
                #value=26786
            )], style={'width': '250px', 'display': 'inline-block', 'margin': '2%'}),

        html.Div([
            html.Label('Runner Name'),
            dcc.Dropdown(id='runner-id')], style={'width': '250px', 'display': 'inline-block', 'margin': '2%'}),
        
    ]),
    
    dcc.Graph(id='adjusted-times')
    
])

@app.callback(
    Output('team-name', 'options'),
    [Input('state', 'value')])
def set_team_options(selected_state):
    df_state_teams = pyscripts.get_teams_in_state(selected_state, es)
    print 'Retrieved %s teams for %s' %(len(df_state_teams), selected_state)
    return [{'label': k, 'value': v} for i, v, k in df_state_teams.itertuples()]

@app.callback(
    Output('runner-id', 'options'),
    [Input('year', 'value'),
    Input('team-name', 'value')])
def set_name_options(year, teamID):
#    if teamID not in locals():
#    	print 'teamID is not set yet'
#    	return

    athleteIDs = pyscripts.get_ath_IDs_for_team(teamID, year, es)
    athlete_names = [pyscripts.get_athlete_name(athleteID, es) for athleteID in athleteIDs]
    athlete_name_IDs = [str(x) + '%' + y for x,y in zip(athleteIDs, athlete_names)]
    print 'Retrieved %s names for teamID = %s' %(len(athlete_names), teamID)
    return [{'label': k, 'value': v} for k, v in zip(athlete_names, athlete_name_IDs)]

@app.callback(
    Output('adjusted-times', 'figure'),
    [Input('year', 'value'),
    Input('runner-id', 'value')])
def create_graph(year, athlete_name_ID, plot_next_year=True):
    # create plot.ly graph of runner's times
    # check to see if variables have been defined
    [athleteID, athlete_name] = athlete_name_ID.split('%')
    print 'athlete ID is %s' %athleteID

    df_runner_data = pyscripts.get_runner_perf_data(athleteID, year, es)
    if not isinstance(df_runner_data, pd.DataFrame):
        print 'not a dataframe'
        return
    
    if len(df_runner_data) == 0:
        print 'no results for athlete'
        return

    # sometimes there are old races, remove them
    def remove_old_races(df):
        # remove race data if EventID says 'old
        return df[~df['EventID'].str.contains('old')]

    #df_runner_data = remove_old_races(df_runner_data)

    # find min time for runner (only use 5K times)
    min_time = df_runner_data[df_runner_data['EventCode'] == '5000m'].Units.min()
    print 'Minimum time for %s is %s ' %(athlete_name, str(min_time))
    
    raceIDs = df_runner_data.EventID.values
    raceID = long(raceIDs[0])
    # convert to type es can serialize
    #handicap_file = str(year) + '_xc_event_handicaps_against_221756_2016.pickle'
    df_hdcap = pyscripts.get_race_hcap(raceID, es)
    print 'pulled handicap information'
    
    # create interval sequence
    bin_sequence = pyscripts.create_bin_seq(df_hdcap)
    
    # find the index in the handicap dataframe that corresponds to the min time
    index = next(x[0] for x in enumerate(bin_sequence) if x[1] > min_time) - 1

    # pull all handicaps and race names
    hdcaps = []
    race_names = []
    race_dates = []
    for raceID in raceIDs:
        raceID = long(raceID) # convert to long to type that can be serialized
        df_hdcap = pyscripts.get_race_hcap(raceID, es)
        hdcaps.append(int(df_hdcap.iloc[index]))
        name, date = pyscripts.get_race_info(raceID, es)
        race_names.append(name)
        race_dates.append(date)
        
    # add colums to the datafrarme to adjust the race time and add the race name
    df_runner_data['Handicap'] = hdcaps
    df_runner_data['RaceName'] = race_names
    df_runner_data['Date'] = race_dates
    df_runner_data['AdjUnits'] = (df_runner_data['Units'] - df_runner_data['Handicap'])
    df_runner_data.sort_values('Date', inplace=True) #.plot(x='Date', y='AdjUnits')
    print 'retrieved %s races for %s' %(len(df_runner_data), str(year))

    
    # create plot
    traces = []
    #x_data = [datetime.datetime.fromtimestamp(x/1000) for x in df_runner_data['Date']]
    x_data = df_runner_data['Date']
    y0_data = [pyscripts.racedatetime(x) for x in df_runner_data['Units']]
    y1_data = [pyscripts.racedatetime(x) for x in df_runner_data['AdjUnits']]
    text = df_runner_data['RaceName']

    traces.append(go.Scatter(
        y=y0_data,
        x=x_data,
        text=text,
        mode='markers+lines',
        connectgaps=True,
        #hoverinfo='lines+markers+text',
        name='Actual Time %s' %str(year)
                 ))
    traces.append(go.Scatter(
        y=y1_data,
        x=x_data,
        text=text,
        mode='markers+lines',
        connectgaps=True,
        #hoverinfo='text+markers',
        name='Adjusted Time %s' %str(year)
                 ))
    
    layout = go.Layout(
        title="%s's Actual and Adjusted Race Times for %s XC Season" %(athlete_name, str(year)),
        xaxis=dict(
            title='Date',
            autorange=True,
            #showgrid=False,
            #zeroline=False,
            #showline=False,
            autotick=True,
            ticks='x',
            tickformat='%-m/%-d'
            #showticklabels=False
        ),
        yaxis=dict(
            title='Time',
            #autorange=True,
            #showgrid=False,
            #zeroline=False,
            #showline=False,
            #autotick=True,
            tickformat='%M:%S',
            hoverformat='%M:%S',
            #showticklabels=False
        )
    )
    if plot_next_year:
        df_runner_data = pyscripts.get_runner_perf_data(athleteID, int(year) + 1, es)

        if len(df_runner_data) != 0:
             # pull all handicaps and race names
             raceIDs = df_runner_data.EventID.values
             hdcaps = []
             race_names = []
             race_dates = []
             for raceID in raceIDs:
                 raceID = long(raceID) # convert to long to type that can be serialized
                 df_hdcap = pyscripts.get_race_hcap(raceID, es)
                 hdcaps.append(int(df_hdcap.iloc[index]))
                 name, date = pyscripts.get_race_info(raceID, es)
                 race_names.append(name)
                 race_dates.append(date)
             print race_names
             # add colums to the datafrarme to adjust the race time and add the race name
             df_runner_data['Handicap'] = hdcaps
             df_runner_data['RaceName'] = race_names
             df_runner_data['Date'] = race_dates
             df_runner_data['AdjUnits'] = (df_runner_data['Units'] - df_runner_data['Handicap'])
             df_runner_data.sort_values('Date', inplace=True) #.plot(x='Date', y='AdjUnits')
             
             # create plot
             #traces = []
             x_data = [ datetime.datetime.strptime(x, "%Y-%m-%dT%H:%M:%S") -
                        datetime.timedelta(364)
                        for x in df_runner_data['Date']]
             #x_data = df_runner_data['Date']
             y0_data = [pyscripts.racedatetime(x) for x in df_runner_data['Units']]
             y1_data = [pyscripts.racedatetime(x) for x in df_runner_data['AdjUnits']]
             text = df_runner_data['RaceName']

             traces.append(go.Scatter(
                 y=y0_data,
                 x=x_data,
                 text=text,
                 mode='markers+lines',
                 connectgaps=True,
                 #hoverinfo='lines+markers+text',
                 name='Actual Time %s' %str(int(year)+1)
                          ))
             traces.append(go.Scatter(
                 y=y1_data,
                 x=x_data,
                 text=text,
                 mode='markers+lines',
                 connectgaps=True,
                 #hoverinfo='text+markers',
                 name='Adjusted Time %s' %str(int(year)+1)
                          ))

    return go.Figure(data=traces, layout=layout)

#if __name__ == '__main__':
#     app.run_server()