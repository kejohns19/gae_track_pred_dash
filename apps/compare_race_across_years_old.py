import pandas as pd
import numpy as np
import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output
import plotly.graph_objs as go
import plotly.figure_factory as ff
from elasticsearch import Elasticsearch
import datetime
import pyscripts

from app import app

#endpoint = '35.196.248.48:9200'
#endpoint = 'http://localhost:8001/api/v1/namespaces/default/services/elasticsearch:9200/proxy'
endpoint = '10.128.0.2:9200' # this is the google compute instance that has es
es = Elasticsearch(endpoint)

#app = dash.Dash(__name__)
#server = app.server # the Flask app
#my_css_url = 'https://codepen.io/chriddyp/pen/bWLwgP.css'
#app.css.append_css({"external_url": my_css_url})

states = ['AL', 'AK', 'AZ', 'AR', 'CA', 'CO', 'CT', 'DE', 'FL', 'GA', 'HI', 'ID', 
             'IL', 'IN', 'IA', 'KS', 'KY', 'LA', 'ME' ,'MD', 'MA', 'MI', 'MN', 'MS', 
             'MO', 'MT', 'NE', 'NV', 'NH', 'NJ', 'NM', 'NY', 'NC', 'ND', 'OH', 'OK', 
             'OR', 'PA', 'RI', 'SC', 'SD', 'TN', 'TX', 'UT', 'VT', 'VA', 'WA', 'WV',
             'WI', 'WY', 'DC']
             
years = ['2017', '2016', '2015', '2014']

layout = html.Div([
    dcc.Link('Go to Index', href='/'),
    html.Div(),
    html.Div([
        html.Div([
            html.H1(
                'HS XC Race Comparision',
                style={'text-align': 'center'},),],),

        html.Div([
            html.Label('State'),
            dcc.Dropdown(
                id='state1',
                options=[{'label': i, 'value': i} for i in states],
                value='VA'
            )], style={'width': '75px', 'display': 'inline-block', 'margin': '.5%'}),

        html.Div([
            html.Label('Base Year'),
            dcc.Dropdown(
                id='year1',
                options=[{'label': i, 'value': i} for i in years],
                #multi=True,
                value='2017'
            )], style={'width': '125px', 'display': 'inline-block', 'margin': '.5%'}),

        html.Div([
            html.Label('Select First Race - This is the Base Race'),
            dcc.Dropdown(
                id='raceID_name1',
            )], style={'width': '400px', 'display': 'inline-block', 'margin': '.5%'}),
 
        html.Div(),
        
        html.Div([
            html.Label('State'),
            dcc.Dropdown(
                id='state2',
                options=[{'label': i, 'value': i} for i in states],
                value='VA'
            )], style={'width': '75px', 'display': 'inline-block', 'margin': '.5%'}),

        html.Div([
            html.Label('Comparison Year'),
            dcc.Dropdown(
                id='year2',
                options=[{'label': i, 'value': i} for i in years],
                value='2017'
            )], style={'width': '125px', 'display': 'inline-block', 'margin': '.5%'}),
        
        html.Div([
            html.Label('Select Second Race - This is the Comparison Race'),
            dcc.Dropdown(
                id='raceID_name2',
            )], style={'width': '400px', 'display': 'inline-block', 'margin': '.5%'}),
 
    ]),
    
    dcc.Graph(id='race-comparison')
    
])

@app.callback(
    Output('raceID_name1', 'options'),
    [Input('state1', 'value'),
     Input('year1', 'value')])
def set_race_options1(state, year):
    df_state_races = pyscripts.get_race_names_for_year(state, year, es)
    df_state_races = df_state_races.sort_values('Event')
    print 'Retrieved %s races for %s' %(len(df_state_races), state)
    return [{'label': k, 'value': v } for i, k, v in df_state_races.itertuples()]

@app.callback(
    Output('raceID_name2', 'options'),
    [Input('state2', 'value'),
     Input('year2', 'value')])
def set_race_options2(state, year):
    df_state_races = pyscripts.get_race_names_for_year(state, year, es)
    df_state_races = df_state_races.sort_values('Event')
    print 'Retrieved %s races for %s' %(len(df_state_races), state)
    return [{'label': k, 'value': v } for i, k, v in df_state_races.itertuples()]

@app.callback(
    Output('race-comparison', 'figure'),
    [Input('year1', 'value'),
    Input('year2', 'value'),
    Input('raceID_name1', 'value'),
    Input('raceID_name2', 'value')])
def create_graph(year1, year2, raceID_name1, raceID_name2):
    # create plot.ly graph of two races
    [eventID_yr1, name_yr1, date_yr1] = raceID_name1.split('%')
    print 'raceID-name-date for base year is %s' %raceID_name1

    # find same event for other year
    #years = int(year2) - int(year1)
    #days = 15 # +/- days to search
    #datetime_yr1 = datetime.datetime.strptime(date_yr1, "%Y-%m-%dT%H:%M:%S")
    #date_lower_range = (datetime_yr1  + datetime.timedelta((years * 364) - days)).isoformat()
    #date_upper_range = (datetime_yr1  + datetime.timedelta((years * 364) + days)).isoformat()
    #name_yr2, eventID_yr2, date_yr2 = pyscripts.get_races_by_name(name_yr1, year2, state, date_lower_range, date_upper_range, es)
    [eventID_yr2, name_yr2, date_yr2] = raceID_name2.split('%')
    print 'race name for comparison year is %s' %name_yr2
    
    df = pyscripts.get_race_data([eventID_yr2, eventID_yr1], es)
    df['EventID'] = df['EventID'].astype('int')
        
    # get year1 race handicap file
    #df_hcap = pyscripts.get_race_hcap(eventID_yr1, es)
    #print 'handicap for base year is:'
    #print df_hcap
    
    # create the bin sequence from the handicap df
    #bins = list(df_hcap.index)
    #bins = sorted(bins)
    #bin_sequence = []
    bin_sequence = [0, 17, 18, 19, 20, 21, 22, 23, 24, 25, 50]
    #for text in bins:
    #    nums = [int(filter(str.isdigit, str(s))) for s in text.split()]
    #    bin_sequence.append(nums[0])
    #nums = [int(filter(str.isdigit, str(s))) for s in text.split()]
    #bin_sequence.append(nums[1])
    
    # find difference between races
    runners_per_event = df.groupby('EventID').AthleteID.count()
    print runners_per_event

    bin_length = 25
    event_fewest = runners_per_event.idxmin()
    runners_fewest = runners_per_event.min()
    # cut off the last portion of runners, it gets wonky and not predictable
    x_length = int(runners_fewest/ 1.1)

    def return_sorted_runner_times(eventID, x_length):
        return df[df['EventID'] == int(eventID)].sort_values('Units')['Units'][:x_length].reset_index(drop=True)

    race1 = return_sorted_runner_times(eventID_yr1, x_length)
    race2 = return_sorted_runner_times(eventID_yr2, x_length)

    df_diff = race1 - race2
    df_mean = (race1 + race2) / 2

    mean_diff_by_bin = df_diff.groupby(df_diff.index / bin_length).mean()
    mean_times_by_bin = df_mean.groupby(df_mean.index / bin_length).mean()  
    
    # adjust the races with the handicap information (for simplilcity just use the year1 hcap info)
    bins_for_cut = (np.array(bin_sequence)*60000) #+ np.append(0, df_hcap.values)
    mean_diff_by_bin.index = pd.cut(mean_times_by_bin, bins=bins_for_cut).apply(pyscripts.reformat_Range)
    mean_diff_table = mean_diff_by_bin.groupby(mean_diff_by_bin.index).mean()
       
    # create plot
    traces = []
    x_data = list(race1.index)
    y0_data = [pyscripts.racedatetime(x) for x in race1]
    y1_data = [pyscripts.racedatetime(x) for x in race2]

    traces.append(go.Scatter(
        y=y0_data,
        x=x_data,
        #text=text,
        mode='lines',
        connectgaps=True,
        hoverinfo='y',
        name='%s: %s' %(str(year1), name_yr1),
        xaxis='x2', yaxis='y2')
               )
    traces.append(go.Scatter(
        y=y1_data,
        x=x_data,
        #text=text,
        mode='lines',
        connectgaps=True,
        hoverinfo='y',
        name='%s: %s' %(str(year2), name_yr2),
        xaxis='x2', yaxis='y2')
                )
    
    
    # create graphic, start with table
    mean_diff_list = [str(round(x/1000,1)) for x in mean_diff_table]
    temp = [(y, z + ' sec') for y,z in zip(mean_diff_table.index, mean_diff_list)]
    fig = ff.create_table([['Time Range<br>(minutes)', 'Difference<br>base-comparison']] + temp, height_constant=40)
    
    # then add plot
    fig['data'].extend(go.Data(traces))
    
    # Edit layout for subplots
    fig.layout.xaxis.update({'domain': [0, .2]})
    fig.layout.xaxis2.update(dict(domain=[0.3, 1.], title='Position', rangemode='tozero', autorange=True))

    # The graph's yaxis MUST BE anchored to the graph's xaxis
    fig.layout.yaxis2.update(dict(anchor='x2', title='Time', tickformat='%M:%S', hoverformat='%M:%S'))
    fig.layout.legend.update(dict(x=.31, y=.95))
    fig_title = 'Time Comparison for Each Runner Position'
    #fig_subtitle = 'There are %s Similar Runners within +/- %s Seconds' %(total_results, similarity)
    table_title = 'Time Difference Between Races'
    fig.layout.margin.update({'t':50, 'b':50, 'r':25, 'l':25})
    fig.layout.annotations.extend([dict(x=.6, y= 1.1, xref='paper', yref='paper', text=fig_title, showarrow=False, font=dict(size=14))])
    #fig.layout.annotations.extend([dict(x=.6, y= 1.08, xref='paper', yref='paper', text=fig_subtitle, showarrow=False)])
    fig.layout.annotations.extend([dict(x=.03, y= 1.1, xref='paper', yref='paper', text=table_title, showarrow=False, font=dict(size=14))])
    
    return fig

#if __name__ == '__main__':
#     app.run_server(debug=True)