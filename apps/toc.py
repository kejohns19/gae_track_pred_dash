from dash.dependencies import Input, Output
import dash_html_components as html
import dash_core_components as dcc

from app import app

layout = html.Div([
    html.H2('Welcome to High School Distance Analytics'),
    dcc.Link('Distance Track Race Projections (800m, 1600m, 3200m)', href='/track-time-prediction', style={'fontSize': 18}),
    html.Div(),
    dcc.Link('High School Cross Country Time Corrections (normalize to flat course)', href='/xc-adjustment-graph', style={'fontSize': 18}),
    html.Div(),
    dcc.Link('Compare Times for Same Race for Two Different Years (compare relative times)', href='/compare-races-across-years', style={'fontSize': 18})

])