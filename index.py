from dash.dependencies import Input, Output
import dash_core_components as dcc
import dash_html_components as html

try:
  import googleclouddebugger
  googleclouddebugger.AttachDebugger()
except ImportError:
  pass

from app import app
server = app.server
from apps import toc, track_time_prediction, xc_adjustment_graph, compare_race_across_years

app.layout = html.Div([
    dcc.Location(id='url', refresh=False),
    html.Div(id='page-content')
])


@app.callback(Output('page-content', 'children'),
              [Input('url', 'pathname')])
def display_page(pathname):
    if pathname == '/':
         return toc.layout
    elif pathname == '/xc-adjustment-graph':
         return xc_adjustment_graph.layout
    elif pathname == '/track-time-prediction':
         return track_time_prediction.layout
    elif pathname == '/compare-races-across-years':
         return compare_race_across_years.layout
    else:
        return html.Div([dcc.Link('Go to Index', href='/')])

if __name__ == '__main__':
    app.run_server()